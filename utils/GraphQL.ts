import config from "@app/config";

export function format(column: string, value: any) {
  if ((config.identifierType as any)[column] === "int") return value;
  else return `"${value}"`;
}

String.prototype.endsWith = function(suffix) {
  return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

export function getUnique(arr: any[], comp: any) {
  const unique = arr
    .map(e => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
}
