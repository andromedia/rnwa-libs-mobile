import {
  createDrawerNavigator,
  DrawerNavigatorConfig,
  NavigationContainer,
  NavigationRouteConfigMap
} from "react-navigation";
import NavContainer from "./NavContainer";

export default (
  routeConfigMap: NavigationRouteConfigMap,
  drawerConfig: DrawerNavigatorConfig = {},
  isRoot: boolean = false
): NavigationContainer => {
  if (isRoot) {
    return NavContainer(
      createDrawerNavigator(routeConfigMap, {
        ...drawerConfig
      })
    );
  }
  
  return createDrawerNavigator(routeConfigMap, {
    ...drawerConfig
  });
};
