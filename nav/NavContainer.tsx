import { createAppContainer } from "react-navigation";
import { NavigationActions } from "react-navigation";

export default createAppContainer;

export const createNavigateTo = function(navigation: any) {
  return function(screen: string) {
    navigation.dispatch(
      NavigationActions.navigate({
        routeName: screen
      })
    );
  };
};
