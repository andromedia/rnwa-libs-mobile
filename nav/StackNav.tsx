import {
  createStackNavigator,
  NavigationRouteConfigMap,
  NavigationContainer
} from "react-navigation";
import { StackNavigatorConfig } from "react-navigation";
import NavContainer from "./NavContainer";

export default (
  routeConfigMap: NavigationRouteConfigMap,
  stackConfig: StackNavigatorConfig = {},
  isRoot: boolean = false
): NavigationContainer => {
  if (isRoot) {
    return NavContainer(
      createStackNavigator(routeConfigMap, {
        ...stackConfig,
        headerMode: "none"
      })
    );
  }

  return createStackNavigator(routeConfigMap, {
    ...stackConfig,
    headerMode: "none"
  });
};
