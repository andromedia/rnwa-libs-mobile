import React from "react";
import { Image } from "react-native";

export default (props: any) => {
  return (
    <Image
      source={props.source}
      style={{ ...props.style, ...props.mobileStyle }}
      resizeMode="contain"
    />
  );
};
