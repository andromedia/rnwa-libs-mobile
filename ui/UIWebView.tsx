import React from "react";
import { WebView } from "react-native-webview";

export default (props: any) => {
  return (
    <WebView
      scalesPageToFit={true}
      bounces={false}
      javaScriptEnabled
      style={props.style}
      source={{
        html: props.source.html
      }}
      automaticallyAdjustContentInsets={false}
    />
  );
};
