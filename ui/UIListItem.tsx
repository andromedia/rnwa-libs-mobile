import React from "react";
import { TouchableOpacity, View } from "react-native";

export default (props: { onPress: any; children?: any; style?: any }) => {
  return (
    <View
      style={{
        paddingHorizontal: 13,
        borderBottomColor: "#ccc",
        borderBottomWidth: 1,
        ...props.style
      }}
    >
      <TouchableOpacity
        style={{
          padding: 10,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between"
        }}
        onPress={props.onPress}
      >
        {props.children}
      </TouchableOpacity>
    </View>
  );
};
